//
//  ViewController.swift
//  demoListView
//
//  Created by javad faghih on 1/18/1400 AP.
//

import UIKit

class ListCell: UICollectionViewCell {
    
    static let reusableIdentifier = "reusableIdentifierForCell"
   
    let lable = UILabel()
    let accessoryImageView = UIImageView()
    let separateView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("not implemented")
    }
    
    
    func configure() {
        
        lable.translatesAutoresizingMaskIntoConstraints = false
        lable.adjustsFontForContentSizeCategory = true
        lable.font = UIFont.preferredFont(forTextStyle: .title1)
        lable.textAlignment = .center
        lable.layer.borderWidth = 0.5
        lable.layer.borderColor = UIColor.black.cgColor
        separateView.translatesAutoresizingMaskIntoConstraints = false
        separateView.backgroundColor = .lightGray
        
        selectedBackgroundView = UIView()
        selectedBackgroundView?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        contentView.addSubview(lable)
        contentView.addSubview(accessoryImageView)
        contentView.addSubview(separateView)
        
        let rtl = effectiveUserInterfaceLayoutDirection == .rightToLeft
        let chevronImageName = rtl ? "chevron.left" : "chevron.right"
        let chevronImage = UIImage(systemName: chevronImageName)
        accessoryImageView.image = chevronImage
        accessoryImageView.tintColor = UIColor.darkGray.withAlphaComponent(0.7)
    
        let inset = CGFloat(10)
        NSLayoutConstraint.activate([
            lable.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: inset),
            lable.topAnchor.constraint(equalTo: contentView.topAnchor, constant: inset),
            lable.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -inset),
            lable.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -inset),
            
            accessoryImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            accessoryImageView.widthAnchor.constraint(equalToConstant: 13),
            accessoryImageView.heightAnchor.constraint(equalToConstant: 20),
            accessoryImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -inset),
            
            separateView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: inset),
            separateView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -inset),
            separateView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            separateView.heightAnchor.constraint(equalToConstant: 0.5),
            

        ])
    }
}


class TextCell: UICollectionViewCell {
    static let reusableIdentifier = "text-cell-reuse-identifier"
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("can not initialized")
    }
    
    func configure() {
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textAlignment = .center
        contentView.addSubview(label)
        
        let inset = CGFloat(10)
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: inset),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -inset),
            label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: inset),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -inset)
        
        ])
    }
}




class ViewController: UIViewController {

    enum Section {
        case main
    }
    
    var dataSource: UICollectionViewDiffableDataSource<Section, Int>! = nil
    var collectionView: UICollectionView! = nil
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHeirarchy()
        configureDataSource()

    }

    
    
    
    private func configureHeirarchy() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isScrollEnabled = false
        collectionView.isPagingEnabled = false
        collectionView.register(TextCell.self, forCellWithReuseIdentifier: TextCell.reusableIdentifier)
        view.addSubview(collectionView)
        
    }
   

    private func createLayout() -> UICollectionViewLayout {
        
        let inset: CGFloat = 20

          // item on top
        let topItemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let topItem = NSCollectionLayoutItem(layoutSize: topItemSize)
            topItem.contentInsets = NSDirectionalEdgeInsets(top: inset, leading: inset + 30, bottom: inset + 20, trailing: inset + 30)

            //Group Top Item
        let topGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1/3))
        let topGroup = NSCollectionLayoutGroup.horizontal(layoutSize: topGroupSize, subitem: topItem, count: 3)
        
        
        
          // Middle Item
          let bottomItemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
          let bottomItem = NSCollectionLayoutItem(layoutSize: bottomItemSize)
          bottomItem.contentInsets = NSDirectionalEdgeInsets(top: inset, leading: inset + 30, bottom: inset + 20, trailing: inset + 30)

          // Group for bottom item, it repeats the bottom
          let bottomGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1/3))
          let bottomGroup = NSCollectionLayoutGroup.horizontal(layoutSize: bottomGroupSize, subitem: bottomItem, count: 3)

          // Combine the top item and bottom group
        let fullGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(1.0))
         
        let nestedGroup = NSCollectionLayoutGroup.vertical(layoutSize: fullGroupSize, subitems: [topGroup, bottomGroup])

          let section = NSCollectionLayoutSection(group: nestedGroup)
              section.orthogonalScrollingBehavior = .groupPaging
        
          let layout = UICollectionViewCompositionalLayout(section: section)

          return layout
    }
    
    
    
    private func configureDataSource() {
        
        dataSource = UICollectionViewDiffableDataSource<Section, Int>(collectionView: collectionView) {
            (collectionView: UICollectionView, indexPath: IndexPath, identifier: Int ) -> UICollectionViewCell? in
        
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TextCell.reusableIdentifier, for: indexPath) as? TextCell else { fatalError("Can not create a new cell")}
            
            
            cell.label.text = "\(identifier)"
            cell.layer.borderWidth = 1.0
            cell.layer.borderColor = UIColor.black.cgColor
            
            return cell
        }
        
        var snapshot = NSDiffableDataSourceSnapshot<Section, Int>()
        snapshot.appendSections([.main])
        snapshot.appendItems(Array(0..<23))
        
        dataSource.apply(snapshot, animatingDifferences: false)
        
    }
    

}

